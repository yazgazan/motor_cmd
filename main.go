
package main

import (
  "io"
  "io/ioutil"
  "fmt"
  "log"
  "time"
  "strconv"
  "strings"
  "net/http"

  "github.com/tarm/goserial"
)

type Command struct{
  Id      uint8
  Enable  uint8
  Way     uint8
  Speed   uint8
  Cb      func (s string) ()
}

func main() {
  c := make(chan Command, 1)
  s := setupSerial()
  handleHttp(c)

  resync(s)
  fmt.Println("Ready for action")

  for {
    ret := ""
    cmd := <-c

    fmt.Printf("new command: %+v\n", cmd)

    cmd_buf := make([]byte, 4)
    cmd_buf[0] = cmd.Id
    cmd_buf[1] = cmd.Enable
    cmd_buf[2] = cmd.Way
    cmd_buf[3] = cmd.Speed
    s.Write(cmd_buf)

    for {
      rep, err := ioutil.ReadAll(s)
      if err != nil {
        if err == io.EOF {
          continue
        }
        ret = "Error, can't get an answer from the motor driver."
        break
      }
      if len(rep) > 0 {
        fmt.Println(string(rep))
        ret += string(rep)
      }
      if strings.Contains(ret, "\n") == true {
        break
      }
    }
    cmd.Cb(ret)
  }
}

func handleHttp(c chan Command) {
  http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
    path := req.URL.Path
    cb_chan := make(chan bool, 1)

    if len(path) < 4 || path[:4] != "/cmd" {
      log.Printf("malformated url: '%s'\n", path)
      return
    }

    var cmd Command

    cmd.Id = readUint8(req.FormValue("id"), 0)
    cmd.Enable = readUint8(req.FormValue("enable"), 0)
    cmd.Way = readUint8(req.FormValue("way"), 0)
    cmd.Speed = readUint8(req.FormValue("speed"), 0)

    cmd.Cb = func (s string) {
      w.Write([]byte(s))
      cb_chan <- true
    }

    c <- cmd
    <-cb_chan
  })

  go func () {
    err := http.ListenAndServe("localhost:4354", nil)
    if err != nil {
      log.Fatal(err)
    }
  } ()
}

func resync(s io.ReadWriteCloser) {
  b := make([]byte, 1)
  var rep []byte
  c := make(chan error, 1)

  b[0] = 0xFF
  write := func() {
    s.Write(b)
  }
  read := func() {
    var e error
    rep, e = ioutil.ReadAll(s)
    c <- e
  }

  write()
  go read()

  for {
    select {
    case err := <-c:
      if err == nil {
        if len(rep) == 0 || strings.Contains(string(rep), "\n") == false {
          go read()
          continue
        }
        return
      }
      if err == io.EOF {
        go read()
        continue
      }
      log.Printf("Error sync: %s\n", err)
      return
    case <- time.After(time.Millisecond * 500):
      write()
    }
  }
}

func setupSerial() io.ReadWriteCloser {
  c := &serial.Config{Name: "/dev/ttyUSB0", Baud: 115200}
  s, err := serial.OpenPort(c)

  if err != nil {
    log.Fatal(err)
  }
  time.Sleep(500 * time.Millisecond)
  return s
}

func readUint8(s string, def uint8) uint8 {
  if s == "" {
    return def
  }
  n, err := strconv.ParseUint(s, 0, 8)
  if err != nil {
    fmt.Printf("parsing error: %s\n", err)
    return def
  }
  return uint8(n)
}

